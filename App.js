import { Button, Checkbox, Form, Input, Radio } from "antd"


const FormSubmit = () => {
  const onFinish = (values) => {
    console.log("onFinish", values)
  }
  return (
    <div>
      <div><h2>Information</h2></div>
      <Form onFinish={onFinish}>
        <Form.Item name="first_name" label="First Name"
          rules={[
            {
              required: true,
              message: 'Please input your first name!',
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item name="last_name" label="Last Name"
          rules={[
            {
              required: true,
              message: 'Please input your last name!',
            },
          ]}>
          <Input />
        </Form.Item>
        <Form.Item name="email" label="Email"
          rules={[
            {
              type: 'email',
              message: 'not valid email!',
              required: 'true'
            },
          ]}>
          <Input />
        </Form.Item>
        <Form.Item
          label="Password"
          name="password"
          rules={[
            {
              required: true,
              message: 'Please input your password!',
            },
          ]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item name={"textarea"} label="Discription"
          rules={[
            {
              required: true,
              message: 'Please input your discription!',
            },
          ]}>

          <Input.TextArea />
        </Form.Item>
        <Form.Item>
          <Checkbox defaultChecked="true" >
            Active
          </Checkbox>
        </Form.Item>
        <Form.Item name="radio-group" label="Gender" rules={[
          {
            required: true,
            message: 'Please input your discription!',
          },
        ]}>
          <Radio.Group>
            <Radio value="male">Male</Radio>
            <Radio value="female">Female</Radio>
            <Radio value="others">Others</Radio>
          </Radio.Group>
        </Form.Item>
        <Form.Item
        name="phoneNumber"
        label="Phone Number"
        rules={[
          {
            pattern: /^[0-9]{10}$/, // Replace this regex with your desired pattern
            message: 'Not a valid phone number!',
          },
          {
            required: true,
            message: 'Please enter your phone number!',
          },
        ]}
      >
        <Input placeholder="Enter your phone number" />
      </Form.Item>
        <Button htmlType="submit">
          Form Submit
        </Button>
      </Form>
    </div>
  )
}
export default FormSubmit