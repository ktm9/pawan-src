import React from "react";

const AboutUs = () => {
  return (
    <div>
      <div className="text-red-600 bg-slate-400 text-5xl flex-none text-center"> About Us</div>
      <p>
        Welcome to VOGUE HUB, your one-stop online shopping destination in Nepal.
      </p>
      <p>
        At VOGUE HUB, we strive to provide our customers with the best online shopping experience by offering a wide range of products from various categories, including electronics, fashion, beauty, home essentials, and more.
      </p>
      <p>
        Our mission is to make shopping convenient, enjoyable, and affordable for everyone across Nepal. We source high-quality products, partner with reliable suppliers, and ensure timely delivery to your doorstep.
      </p>
      <p>
        VOGUE HUB is not just a marketplace; it's a community. We value our customers and work hard to exceed their expectations. Your satisfaction is our priority, and we are committed to building long-lasting relationships with our customers.
      </p>
      <p>
        Thank you for choosing VOGUE HUB. Happy shopping!
      </p>
    </div>
  );
};

export default AboutUs;