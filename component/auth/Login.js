import React from 'react';
import { Form, Input, Button, Checkbox, Card } from 'antd';
import { Link, useNavigate } from 'react-router-dom';
import { auth } from '../../utils/Items';

const LoginForm = () => {
  const navigate = useNavigate();
  const data = auth[0];

  const onFinish = (values) => {
    console.log('values', values);

    if (values?.user_name === data.type) {
      navigate('/');
    } else {
      console.log("Admin login logic goes here");
      navigate('/admin');
    }
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
      <Card style={{ width: '300px' }}>
        <div style={{ display: 'flex', justifyContent: 'center', marginBottom: '20px' }}>
          <h2 style={{ fontSize: '1.5rem', margin: '0' }}>Log In</h2>
        </div>
        <Form
          name="login-form"
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
        >
          <Form.Item
            label="Email"
            name="email"
            rules={[
              {
                required: true,
                message: 'Please input your email!',
              },
              {
                type: 'email',
                message: 'Invalid email address',
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Password"
            name="password"
            rules={[
              {
                required: true,
                message: 'Please input your password!',
              },
            ]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item name="remember" valuePropName="checked">
            <Checkbox>Remember me</Checkbox>
          </Form.Item>

          <Form.Item>
            <div style={{ textAlign: 'center' }}>
              <Button style={{ width: '100%', background: '#1890ff', color: 'white' }} htmlType="submit">
                Log in
              </Button>
              <div style={{ marginTop: '10px' }}>
                <Link to="/auth/signup" style={{ color: '#1890ff' }}>Don't have an account? Sign Up</Link>
              </div>
            </div>
          </Form.Item>
        </Form>
      </Card>
    </div>
  );
};

export default LoginForm;
