import React from "react";
import { Button, Card, Form, Input } from "antd";
import { Link } from "react-router-dom";

const UserSignUpForm = () => {
  const onFinish = (values) => {
    console.log("values", values);
  };

  return (
    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
      <Card style={{ width: '400px', padding: '20px', borderRadius: '8px', boxShadow: '0 4px 8px rgba(0, 0, 0, 0.1)' }}>
        <div style={{ textAlign: 'center', marginBottom: '20px' }}>
          <h2 style={{ fontSize: '1.5rem', margin: '0' }}>Sign Up</h2>
        </div>

        <Form onFinish={onFinish} labelCol={{ span: 8 }} wrapperCol={{ span: 16 }}>
          <Form.Item
            name="first_name"
            label="First Name"
            rules={[
              {
                required: true,
                message: "Please input your firstname!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            name="middle_name"
            label="Middle Name"
            rules={[
              {
                required: true,
                message: "Please input your middlename!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            name="last_name"
            label="Last Name"
            rules={[
              {
                required: true,
                message: "Please input your lastname!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            name="phoneNumber"
            label="Phone Number"
            rules={[
              {
                type: "string",
                required: true,
                message: "Please enter your valid Phone Number!",
                pattern: /^[0-9]{10}$/, // Adjust the pattern based on your phone number requirements
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            name="email"
            label="Email"
            rules={[
              {
                type: "email",
                required: true,
                message: "Please enter your valid Email!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            name="password"
            label="Password"
            rules={[
              {
                required: true,
                message: "Please enter your Password!",
              },
            ]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item
            name="retype_password"
            label="Retype Password"
            dependencies={['password']}
            hasFeedback
            rules={[
              {
                required: true,
                message: 'Please retype your password!',
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue('password') === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject('The two passwords do not match!');
                },
              }),
            ]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
            <div style={{ marginBottom: '10px' }}>
              <Button type="primary" htmlType="submit" style={{ width: '100%' }}>
                Sign Up
              </Button>
            </div>
            <div style={{ textAlign: 'center' }}>
              <Link to="/auth/login" style={{ color: '#1890ff' }}>Already have an account? Log In</Link>
            </div>
          </Form.Item>
        </Form>
      </Card>
    </div>
  );
};

export default UserSignUpForm;
