import React from 'react'
import UserItems from './UserItems'
import { HotProductData } from '../../utils/Items'

const HotProducts = () => {
  return (
    <div>
        <UserItems data={HotProductData} title="Hot Products"/>
    </div>
  )
}

export default HotProducts