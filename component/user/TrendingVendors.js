import React from 'react'
 import UserItems from './UserItems'
import { HotProductData } from '../../utils/Items'


const TrendingVendors = () => {
  return (
    <div>
        <UserItems data={HotProductData} title={"Trending Vendors"}/>
    </div>
  )
}

export default TrendingVendors