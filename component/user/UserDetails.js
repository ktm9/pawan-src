import { EyeOutlined } from '@ant-design/icons'
import { Card, Rate } from 'antd'
import React from 'react'
import { useAppContext } from '../../contextApi'

const UserDetails = () => {
    const {appState}= useAppContext();
    console.log("userdetails",appState);
 
    return (
        <div  style={{display:'flex', justifyContent:'flex-start', gap:'4px'}}>
            <Card
                hoverable
                style={{ width: 240 }}
                cover={<img alt="example"
                    src={
                        appState.detail.image} />}
            >
                <div>Price: {
                appState.detail.price}</div>
                <div>Brand: {
                appState.detail.brand}</div>
            </Card>
           <div>
           <div>{
           appState.detail?.name}</div>
            <div>Rate:<Rate value={
                appState.detail?.name}/></div>
            <div>{
            appState.detail?.description}</div>
            <div>{
            appState.detail?.stokItem}</div>       
            <div><EyeOutlined/>{appState?.detail.view}People viewed this Product</div>
           </div>


        </div>
    )
}

export default UserDetails