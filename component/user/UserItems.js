import { Button, Card } from 'antd'
import React from 'react'
import { useNavigate } from 'react-router-dom'
import { useAppContext } from '../../contextApi'

const UserItems = ({ data, title }) => {
    const { appState, updateState } = useAppContext();
    const navigate = useNavigate()
    const handleClick = (item) => {
        updateState({
            ...appState,
            detail: item
        });
        navigate(`/userdetails/${item.id}`)
        updateState(item);
        localStorage.setItem("/userdetails", JSON.stringify(item))
    }
    const addToCard = (item) => {
        updateState({
            ...appState,
            addtocard: [...appState?.addtocard,...[item]]
        })
    }
    return (
        <div>
            <div style={{
                fontFamily: 'Arial, sans-serif',
                fontSize: '50px',
            }}>
                {title}
            </div>
            <div>
                {
                    <div style={{ display: 'flex', justifyContent: 'flex-start', gap: '4px' }}>
                        {
                            data?.map((Item) => (
                                <div key={Item.id} >
                                        
                                    <Card hoverable >
                                    <Card
                                        onClick={() => handleClick(Item)}
                                        style={{ width: 240 }}
                                        cover={<img alt="example"
                                            src={Item.image} />}
                                    >
                                        <div>Price: {Item.price}</div>
                                        <div>Brand: {Item.brand}</div>
                                    </Card>
                                    <div> <Button className='w-full ' onClick={() => addToCard(Item)}>Add To Cart</Button></div>
                                    </Card>
                                 
                                    
                                </div>
                            ))
                        }
                    </div>
                }
            </div>
        </div >
    )
}

export default UserItems