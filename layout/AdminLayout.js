import React from 'react';
import { LaptopOutlined, NotificationOutlined, UserOutlined } from '@ant-design/icons';
import { Layout, Menu } from 'antd';
import { Outlet } from 'react-router-dom';
import AdminHeader from './header/AdminHeader';

const { Header, Content, Footer, Sider } = Layout;

const items2 = [UserOutlined, LaptopOutlined, NotificationOutlined].map((icon, index) => {
  const key = String(index + 1);
  return {
    key: `sub${key}`,
    icon: React.createElement(icon),
    label: `yagya ${key}`,
    children: new Array(4).fill(null).map((_, j) => {
      const subKey = index * 4 + j + 1;
      return {
        key: subKey,
        label: `option${subKey}`,
      };
    }),
  };
});

const AdminLayout = () => {
  return (
    <div>
      <Layout>
        <Header style={{ color: 'white' }}>
          <AdminHeader/>
          <div className="demo-logo" />
        </Header>
        <Content style={{ padding: '0 48px' }}>
         
          <Layout
            style={{
              padding: '24px 0',
              background: '#f0f2f5',
              borderRadius: '8px',
            }}
          >
            <Sider style={{ background: '#f0f2f5' }} width={200}>
              <Menu
                mode="inline"
                defaultSelectedKeys={['1']}
                defaultOpenKeys={['sub1']}
                style={{ height: '100%' }}
                items={items2}
              />
            </Sider>
            <Content style={{ padding: '0 24px', minHeight: 280 }}>
              <Outlet />
            </Content>
          </Layout>
        </Content>
        <Footer
          style={{
            textAlign: 'center',
            background: '#001529',
            color: 'white',
            padding: '20px',
          }}
        >
          <div>
            <h2>Active Footwear</h2>
            <p>Quality Shoes for an Active Lifestyle</p>
          </div>
          <div>
            <p>Contact: info@activefootwear.com</p>
            <p>&copy; {new Date().getFullYear()} Active Footwear. All rights reserved.</p>
          </div>
        </Footer>
      </Layout>
    </div>
  );
};

export default AdminLayout;
