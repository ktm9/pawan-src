import { Content, Footer, Header } from 'antd/es/layout/layout'
import React from 'react'
import { Outlet } from 'react-router-dom'
import Mainheader from './header'
import { Layout } from 'antd'

 const UserLayout = () => {
 
   
  return (
    <div>
    
    <Layout>
      <Header
        style={{color:'white'
        }}
      >
        <Mainheader/>
        <div className="demo-logo"/>
        
      </Header>
      <Content
        style={{
          padding: '0 48px',
        }}
      >
   
        <Layout
        
        >
        
          <Content
            style={{
              padding: '0 24px',
              minHeight: 280,
            }}
          >
          <Outlet/>
          </Content>
        </Layout>
      </Content>
      <Footer
          style={{
            textAlign: 'center',
            background: '#001529',
            color: 'white',
            padding: '20px',
          }}
        >
          <div>
            <h2>Active Footwear</h2>
            <p>Quality Shoes for an Active Lifestyle</p>
          </div>
          <div>
            <p>Contact: info@activefootwear.com</p>
            <p>&copy; {new Date().getFullYear()} Active Footwear. All rights reserved.</p>
          </div>
        </Footer>
    </Layout>

  </div>
  )
}
export default UserLayout