import React from 'react';
import { Button, Menu } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import Dropdown from 'antd/es/dropdown/dropdown';
import { useNavigate } from 'react-router-dom';

const AdminHeader = () => {
  const navigate = useNavigate();

  const handleClick = (e) => {
    console.log('e', e.key);
    
    if (e.key === 'logout') {
      localStorage.removeItem('token');
      navigate('/auth/login');
    } else if (e.key === 'profile') {
      // Handle Profile button click
      console.log('Profile button clicked');
      // Add navigation logic for the profile button (e.g., navigate to the home file)
      navigate('/');
    }
  };

  const items = [
    {
      label: 'Log Out',
      key: 'logout',
      danger: true,
    },
    {
      label: 'Profile',
      key: 'profile',
    },
  ];

  const menu = (
    <Menu onClick={handleClick}>
      {items.map((item) => (
        <Menu.Item key={item.key} danger={item.danger}>
          {item.label}
        </Menu.Item>
      ))}
    </Menu>
  );

  return (
    <div className='flex justify-between items-center'>
      <div>
        Logo
      </div>
      <div>
        <Dropdown overlay={menu}>
          <Button icon={<UserOutlined style={{ fontSize: '20px', color: '#1890ff' }} />} />
        </Dropdown>
      </div>
    </div>
  );
};

export default AdminHeader;
