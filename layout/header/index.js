import React from 'react'
import { Auth, headerItems } from '../../utils/Items'
import { useNavigate } from 'react-router-dom'
import logo from '../../image/logo.png'
import Item from 'antd/es/list/Item'
import { ShoppingCartOutlined } from '@ant-design/icons'
import { useAppContext } from '../../contextApi'
import { Badge } from 'antd'
const Index = () => {
    const navigate = useNavigate()
    const handleClick = (e) => {
        console.log('data', e)
        navigate(e)
    }
    const {appState}= useAppContext();
console.log("appState", appState)
    return (
        <div className='flex justify-between'>
            <div key="home"  onClick={()=> handleClick(Item.link)} >
                <img src={logo} alt="Logo" style={{ height: '90px',  width:'100px', margin:'auto'}} />
                
            </div>
            <div style={{ display: 'flex', gap: '40px' }}>
                {
                    headerItems?.map((item) => (
                        <div key={item.link} style={{ color: 'white' }} onClick={() => handleClick(item.link)}>
                            {item.name}
                        </div>
                    ))
                }
            </div>
            <div style={{ display: 'flex', gap: '40px' }}>
            <div >
            <div>
                <Badge count={appState?.addtocard.length}>
                <ShoppingCartOutlined/>

                </Badge>
            </div>
            </div>
                {
                    Auth?.map((item) => (
                        <div key={item.link} style={{ color: 'white' }} onClick={() => handleClick(item.link)}>
                            {item.name}
                        </div>
                    ))
                }
            </div>
        </div>
    )
}

export default Index
