import { Route, createBrowserRouter, createRoutesFromElements } from "react-router-dom";
import AdminLayout from "../layout/AdminLayout";
import AdminDashboard from "../component/admin/AdminDashboard";
import UserLayout from "../layout/UserLayout";
import Aboutus from "../component/aboutus/Aboutus";
import UserDashboard from "../component/user/UserDashboard";
import PagenotFound from "../component/PagenotFound";
import UserDetails from "../component/user/UserDetails";
import AuthLayout from "../layout/AuthLayout";
import SignUp from "../component/auth/SignUp";
import Login from "../component/auth/Login";
import HotProducts from "../component/user/HotProducts";
import TrendingVendors from "../component/user/TrendingVendors";

export const Routers = createBrowserRouter(
    createRoutesFromElements(
        <Route>

            <Route path="/" element={<UserLayout />} >
                <Route index element={<UserDashboard />} />
                <Route path="/aboutus" element={<Aboutus />} />
                <Route path="userdetails/:id" element={<UserDetails />} />
                <Route path="/hotproducts" element={<HotProducts />} />
                <Route path="/trendingvendors" element={<TrendingVendors />} />
                <Route path="auth" element={<AuthLayout />} >
                <Route path="login" element={<Login />} />
                <Route path="signup" element={<SignUp />} />
                </Route>
            </Route>
            <Route>
                <Route path="/admin" element={<AdminLayout />} />
                <Route index element={<AdminDashboard />} />
            </Route>
            <Route path="*" element={<PagenotFound />} />
          

            </Route>
            )
            );
