export const headerItems = [
    {
        name: "About Us",
        link: "/aboutus"
    },
    {
        name: "Contact Us",
        link: "/contactus"
    },
    {
        name: "Hot Products",
        link: "/hotproducts"
    },
    {
        name: "Trending Vendors",
        link: "/trendingvendors"
    },

]
export const Auth = [
    {
        name: "Sign Up",
        link: "/auth/signup"
    },
    {
        name: "Log In",
        link: "/auth/login"
    }, 
]
export const HotProductData = [
    {
        name: "Shoes",
        price: "2690",
        brand:"YR",
        image:"https://calibershoes.sgp1.digitaloceanspaces.com/uploads/2023/09/05122056/727-WT-1.jpg",
        id:1,
        rate:4,
        description:"Bluetooth Calling Smartwatch, IP68 Waterproof, Always On Display with Zinc Alloy Meta Frame Smart Watch",
        stokItem:786,
        view:2344
    },
    {
        name: "T-shirt",
        price: "2690",
        brand:"YR",
        image:"https://calibershoes.sgp1.digitaloceanspaces.com/uploads/2023/09/05122056/727-WT-1.jpg",
        id:2,
        rate:4,
        description:"Bluetooth Calling Smartwatch, IP68 Waterproof, Always On Display with Zinc Alloy Meta Frame Smart Watch",
        stokItem:786,
        view:2344
    },
    {
        name: "T-shirt",
        price: "2690",
        brand:"YR",
        image:"https://calibershoes.sgp1.digitaloceanspaces.com/uploads/2023/09/05122056/727-WT-1.jpg",
        id:3,
        rate:4,
        description:"Bluetooth Calling Smartwatch, IP68 Waterproof, Always On Display with Zinc Alloy Meta Frame Smart Watch",
        stokItem:786,
        view:2344
    },
    {
        name: "T-shirt",
        price: "2690",
        brand:"YR",
        image:"https://calibershoes.sgp1.digitaloceanspaces.com/uploads/2023/09/05122056/727-WT-1.jpg",
        id:4, rate:4,
        description:"Bluetooth Calling Smartwatch, IP68 Waterproof, Always On Display with Zinc Alloy Meta Frame Smart Watch",
        stokItem:786,
        view:2344
    },
]
export const UserItemsData = [
    {
        name: "Shoess",
        price: "2590",
        brand:"YR",
        image:"https://calibershoes.sgp1.digitaloceanspaces.com/uploads/2023/09/05122056/727-WT-1.jpg",
        id:1
    },
    {
        name: "T-shirt",
        price: "2690",
        brand:"YR",
        image:"https://calibershoes.sgp1.digitaloceanspaces.com/uploads/2023/09/05122056/727-WT-1.jpg",
        id:2
    },
    {
        name: "T-shirt",
        price: "2690",
        brand:"YR",
        image:"https://calibershoes.sgp1.digitaloceanspaces.com/uploads/2023/09/05122056/727-WT-1.jpg",
        id:3
    },
    {
        name: "T-shirt",
        price: "2690",
        brand:"YR",
        image:"https://calibershoes.sgp1.digitaloceanspaces.com/uploads/2023/09/05122056/727-WT-1.jpg",
        id:4
    },
]
export const auth = [
    {
        name: "ktm",
        contact: "2590656464",
        email:"abc@gmail.com",
        id:1,
        token:"6uMuSSS0TS8&list=UULFO3pnXIvjeZdn8pihkVqkKQ&index=3",
        type:"user"
    },
]